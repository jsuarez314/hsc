#!/usr/bin/env python
# coding: utf-8

# In[2]:


import astropy, time, sys, os, requests, json

# from PIL import Image
from io import BytesIO

#Astropy
from astropy.table import Table, join
from astropy.io import ascii
from astropy.io import fits 
astropy.conf.max_width = 150

#Common
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

#Progress Bar
from tqdm import tqdm


#For MPI
from numba import jit
from mpi4py import MPI
#@jit


# ### Hubble Catalog of Variables Notebook (API version)
# ###  Useful functions
# 
# * The `hcvcone(ra,dec,radius [,keywords])` function searches the HCV catalog near a position.
# * The `hcvsearch()` function performs general non-positional queries.
# * The `hcvmetadata()` function gives information about the columns available in a table. 
# * The `resolve(name)` function uses the MAST Name Resolver (which relies on both SIMBAD and NED) to get the RA,Dec position for an object.

# In[2]:


hscapiurl = "https://catalogs.mast.stsci.edu/api/v0.1/hsc"

def hcvcone(ra,dec,radius,table="hcvsummary",release="v3",format="csv",magtype="magaper2",
            columns=None, baseurl=hscapiurl, verbose=False,
            **kw):
    """Do a cone search of the HSC catalog (including the HCV)
    
    Parameters
    ----------
    ra (float): (degrees) J2000 Right Ascension
    dec (float): (degrees) J2000 Declination
    radius (float): (degrees) Search radius (<= 0.5 degrees)
    table (string): hcvsummary, hcv, summary, detailed, propermotions, or sourcepositions
    release (string): v3 or v2
    magtype (string): magaper2 or magauto (only applies to summary table)
    format: csv, votable, json
    columns: list of column names to include (None means use defaults)
    baseurl: base URL for the request
    verbose: print info about request
    **kw: other parameters (e.g., 'numimages.gte':2)
    """
    
    data = kw.copy()
    data['ra'] = ra
    data['dec'] = dec
    data['radius'] = radius
    return hcvsearch(table=table,release=release,format=format,magtype=magtype,
                     columns=columns,baseurl=baseurl,verbose=verbose,**data)


def hcvsearch(table="hcvsummary",release="v3",magtype="magaper2",format="csv",
              columns=None, baseurl=hscapiurl, verbose=False,
           **kw):
    """Do a general search of the HSC catalog (possibly without ra/dec/radius)
    
    Parameters
    ----------
    table (string): hcvsummary, hcv, summary, detailed, propermotions, or sourcepositions
    release (string): v3 or v2
    magtype (string): magaper2 or magauto (only applies to summary table)
    format: csv, votable, json
    columns: list of column names to include (None means use defaults)
    baseurl: base URL for the request
    verbose: print info about request
    **kw: other parameters (e.g., 'numimages.gte':2).  Note this is required!
    """
    
    data = kw.copy()
    if not data:
        raise ValueError("You must specify some parameters for search")
    if format not in ("csv","votable","json"):
        raise ValueError("Bad value for format")
    url = "{}.{}".format(cat2url(table,release,magtype,baseurl=baseurl),format)
    if columns:
        # check that column values are legal
        # create a dictionary to speed this up
        dcols = {}
        for col in hcvmetadata(table,release,magtype)['name']:
            dcols[col.lower()] = 1
        badcols = []
        for col in columns:
            if col.lower().strip() not in dcols:
                badcols.append(col)
        if badcols:
            raise ValueError('Some columns not found in table: {}'.format(', '.join(badcols)))
        # two different ways to specify a list of column values in the API
        # data['columns'] = columns
        data['columns'] = '[{}]'.format(','.join(columns))

    r = requests.get(url, params=data)

    if verbose:
        print(r.url)
    r.raise_for_status()
    if format == "json":
        return r.json()
    else:
        return r.text


def hcvmetadata(table="hcvsummary",release="v3",magtype="magaper2",baseurl=hscapiurl):
    """Return metadata for the specified catalog and table
    
    Parameters
    ----------
    table (string): hcvsummary, hcv, summary, detailed, propermotions, or sourcepositions
    release (string): v3 or v2
    magtype (string): magaper2 or magauto (only applies to summary table)
    baseurl: base URL for the request
    
    Returns an astropy table with columns name, type, description
    """
    url = "{}/metadata".format(cat2url(table,release,magtype,baseurl=baseurl))
    r = requests.get(url)
    r.raise_for_status()
    v = r.json()
    # convert to astropy table
    tab = Table(rows=[(x['name'],x['type'],x['description']) for x in v],
               names=('name','type','description'))
    return tab


def cat2url(table="hcvsummary",release="v3",magtype="magaper2",baseurl=hscapiurl):
    """Return URL for the specified catalog and table
    
    Parameters
    ----------
    table (string): hcvsummary, hcv, summary, detailed, propermotions, or sourcepositions
    release (string): v3 or v2
    magtype (string): magaper2 or magauto (only applies to summary table)
    baseurl: base URL for the request
    
    Returns a string with the base URL for this request
    """
    checklegal(table,release,magtype)
    if table == "summary":
        url = "{baseurl}/{release}/{table}/{magtype}".format(**locals())
    else:
        url = "{baseurl}/{release}/{table}".format(**locals())
    return url


def checklegal(table,release,magtype):
    """Checks if this combination of table, release and magtype is acceptable
    
    Raises a ValueError exception if there is problem
    """
    
    releaselist = ("v2", "v3")
    if release not in releaselist:
        raise ValueError("Bad value for release (must be one of {})".format(
            ', '.join(releaselist)))
    if release=="v2":
        tablelist = ("summary", "detailed")
    else:
        tablelist = ("summary", "detailed", "propermotions", "sourcepositions",
                    "hcvsummary", "hcv")
    if table not in tablelist:
        raise ValueError("Bad value for table (for {} must be one of {})".format(
            release, ", ".join(tablelist)))
    if table == "summary":
        magtypelist = ("magaper2", "magauto")
        if magtype not in magtypelist:
            raise ValueError("Bad value for magtype (must be one of {})".format(
                ", ".join(magtypelist)))


def resolve(name):
    """Get the RA and Dec for an object using the MAST name resolver
    
    Parameters
    ----------
    name (str): Name of object

    Returns RA, Dec tuple with position
    """

    resolverRequest = {'service':'Mast.Name.Lookup',
                       'params':{'input':name,
                                 'format':'json'
                                },
                      }
    resolvedObjectString = mastQuery(resolverRequest)
    resolvedObject = json.loads(resolvedObjectString)
    # The resolver returns a variety of information about the resolved object, 
    # however for our purposes all we need are the RA and Dec
    try:
        objRa = resolvedObject['resolvedCoordinate'][0]['ra']
        objDec = resolvedObject['resolvedCoordinate'][0]['decl']
    except IndexError as e:
        raise ValueError("Unknown object '{}'".format(name))
    return (objRa, objDec)


def mastQuery(request, url='https://mast.stsci.edu/api/v0/invoke'):
    """Perform a MAST query.

    Parameters
    ----------
    request (dictionary): The MAST request json object
    url (string): The service URL

    Returns the returned data content
    """
    
    # Encoding the request as a json string
    requestString = json.dumps(request)
    r = requests.post(url, data={'request': requestString})
    r.raise_for_status()
    return r.text


# ### To Download the Image

# In[3]:


def get_hla_cutout(imagename,ra,dec,size=64):
    
    """Get FITS cutout for an image"""
    url = "https://hla.stsci.edu/cgi-bin/fitscut.cgi"
    r = requests.get(url, params=dict(ra=ra, dec=dec, size=size, 
            format="fits", red=imagename))
    im=fits.open(BytesIO(r.content))[0].data
    return im


# In[4]:


#rootpath="/home/toshiba/Development/Transients/"
rootpath="/home/jfsuarez/"

filecatalog_var=rootpath+"HSC/config/hlsp_hcv_hst_wfpc2-acs-wfc3_all_multi_v1_var-cat.dat"
filecatalog_cons=rootpath+"HSC/config/hlsp_hcv_hst_wfpc2-acs-wfc3_all_multi_v1_const-cat-readme.txt"

#path_var=rootpath+"HSC/VAR/"
#path_cons=rootpath+"HSC/CONS/"

path_var="/mnt/HSC_Data/VAR/"
path_cons="/mnt/HSC_Data/CONS/"

path_config=rootpath+"HSC/config/"

if not os.path.exists('downloaded.dat'):
    os.mknod('downloaded.dat')

if not os.path.exists('downloaded_cons.dat'):
    os.mknod('downloaded_cons.dat')

# In[5]:


MATCHID=[]
ACLASS=[]
ECLASS=[]

with open(filecatalog_var) as infile:
    for line in infile:
        MATCHID.append(line.split()[2])
        ACLASS.append(line.split()[5])
        ECLASS.append(line.split()[6])  


# In[6]:


#print(len(MATCHID))
#print(len(ACLASS))


# In[7]:


nobj=len(MATCHID)
if not(os.path.isdir(path_var)):
    os.system("mkdir "+path_var)

if not(os.path.isdir(path_cons)):
    os.system("mkdir "+path_cons)    
# print(MATCHID_DOWN)

# In[ ]:


def download(VAR,rank,MATCHID_UNI,begin,end):
    
    MATCHID_UNI=MATCHID_UNI[begin:end]

    if VAR==True:
        MATCHID_DOWN=np.loadtxt("downloaded.dat",unpack=True).astype(int).astype(str)
    else:
        MATCHID_DOWN=np.loadtxt("downloaded_cons.dat",unpack=True).astype(int).astype(str)
    
    MATCHID_UNI=np.setdiff1d(MATCHID_UNI, MATCHID_DOWN.T) #Quit to create the dataframe

    cols=['MATCHID','RA','DEC','NAMEFILE','ACLASS','ECLASS','StartTime','StopTime','StartMJD','StopMJD','XImage','YImage','N_Images']
    pbar = tqdm(total=len(MATCHID_UNI),desc="Rank"+str(rank))
    
    df=pd.DataFrame(columns=cols)
    
    ip=begin
    imsize1=64
    
    for ID in MATCHID_UNI:
        # Using the detailed table
        notexist=not(ID in MATCHID_DOWN)
        object_seq = ascii.read(hcvsearch(table='detailed',MatchID=ID))
    
#         print(object_seq.info)
   
        mra = object_seq['MatchRA'][0]
        mdec = object_seq['MatchDec'][0]

        if VAR==True:
            path_object=path_var+str(ID)
        else:
            path_object=path_cons+str(ID)
            
        if notexist:
            os.system("mkdir "+path_object)
        nseq=len(object_seq)
        for k in range(nseq):
            if notexist:
                img = get_hla_cutout(object_seq['ImageName'][k],mra,mdec,size=imsize1)
                namefile=path_object+"/"+str(object_seq['ImageName'][k])
                np.save(namefile+".npy", img)
            data=pd.DataFrame([[ID,mra,mdec,object_seq['ImageName'][k],
                                ACLASS[ip],ECLASS[ip],
                                object_seq['StartTime'][k],object_seq['StopTime'][k],
                                object_seq['StartMJD'][k],object_seq['StopMJD'][k],
                                object_seq['XImage'][k],object_seq['YImage'][k],
                                nseq ]],columns=cols)
            #df=df.append([data],ignore_index=True)
        ip += 1
        if notexist:
            if VAR==True:
                os.system("echo "+str(ID)+" >> downloaded.dat")
            else:
                os.system("echo "+str(ID)+" >> downloaded_cons.dat")
        pbar.update(1)
    df.to_pickle('HSC_var_rank'+str(rank)+'_T'+str(time.time())+'.pkl')


# In[8]:


MATCHID_UNI=np.unique(MATCHID)
# MATCHID_UNI=np.setdiff1d(MATCHID_UNI, MATCHID_DOWN.T)

VAR=True # If false download Constant objects

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
#print(rank,size)
nobj2 = int(nobj/size)
#print(nobj2)
download(VAR,rank,MATCHID_UNI,rank*nobj2,(rank+1)*nobj2)
